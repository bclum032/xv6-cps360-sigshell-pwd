#include "types.h"
#include "user.h"
#include "hash.h"
#include "login.h"
#include "user_tools.h"

void login() {
    if (0 != getuid()) {
        printf(1, "Error: not running as root\n");
        exit();
    }

    printf(2, "***Login***\n");
    printf(2, "Default username / password: 'user' / 'password'\n");
    //int fd = open("/users",0);
    //int n, pid;
    char username[9], password[33];
    //char salt[32], hash[32];
    //char entry[80];
    char *argv[] = { "sh", 0 };
    int i=3;
    while(i--) {
        get_username(username);
        int uid = chk_username(username);
        if (uid < 0) {
            printf(1, "Error: invalid username\n");
            continue;
        }

        get_password(password);
        

        if (check_pass(uid, password) == 0) {
            printf(1, "login failed\n");
            continue;
        } else {
            setuid(uid);
            exec("sh", argv);
            printf(1, "login: exec sh failed\n");
            exit();    
        }
    }
    printf(1, "3 failed login attempts, exiting\n");
    exit();
}

int main(int argc, char *argv[]) {
    login();
    exit();
}